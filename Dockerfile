FROM openjdk:8u111-jdk-alpine
EXPOSE 80
ADD /target/Customer-0.0.2-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]

