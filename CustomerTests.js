const newman = require('newman'); // require newman in your project
const fs = require('fs');


let directory = "Consumer-DrivenTests";
let directoryBuffer=Buffer.from(directory);
let files=fs.readdirSync(directory);

    files.forEach(function (file){
      let absFile="./Consumer-DrivenTests/"+`${file}`;
      console.log(absFile);

    newman.run({
    collection: require(absFile),
    reporters: 'cli'
}).on('request', function (error, data) {
    if (error) {
        console.error(error);
    }
    else {
        console.log(data.response.stream.toString());
        fs.writeFile(`response.json`, data.response.stream.toString(), function (error) {
            if (error) {
                console.error(error);
            }
        });
    }
});


});

// call newman.run to pass `options` object and wait for callback

