package thoughtworks.vodqa.Customer;

import java.util.List;


//import org.springframework.beans.factory.annotation.Autowired;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/customers")
public class CustomerController {

	private static final Logger LOGGER = LoggerFactory.getLogger("CustomerController");

	private Customerdata customerdata= new Customerdata();;
	
	@GetMapping
	
	public List<Customer> getCustomers()
	{
		return customerdata.findAll();
	}
	
	
	@GetMapping(value = "/{id}")
	public Customer getCustomerById(@PathVariable("id") int id)
	{
		System.out.println("*****calling customer API*******");
		LOGGER.debug("Called by tests?? ");
		return customerdata.findById(id);
	}
	

}
