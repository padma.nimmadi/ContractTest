package thoughtworks.vodqa.Customer;

public class Contact {
    long telephoneNumber;
    String shippingAddress;

    public Contact(long telephoneNumber, String shippingAddress) {
        this.telephoneNumber = telephoneNumber;
        this.shippingAddress = shippingAddress;
    }

    public long getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(long telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }
}
