package thoughtworks.vodqa.Customer;

import java.util.ArrayList;
import java.util.List;

public class Customerdata {
	
	private List<Customer> customers;

    public Customerdata() {
        customers = new ArrayList<>();
        List<Order> orders1= new ArrayList<>();
        orders1.add(new Order(123,"payment pending"));
        orders1.add(new Order(124,"delivered"));
        orders1.add(new Order(125,"on the way"));

        customers.add(new Customer(1,"John","Doe",3,300,new Contact(1234567890,"Flat No3,4th Main,Bangaalore"),130,orders1));
        List<Order> orders2= new ArrayList<>();
        orders2.add(new Order(223,"payment pending"));
        orders2.add(new Order(224,"delivered"));
        orders2.add(new Order(225,"on the way"));

        customers.add(new Customer(2,"Geoge","Doe",5,500,new Contact(1334567890,"Flat No 41,8th Main,Bangaalore"),100,orders2));
        List<Order> orders3= new ArrayList<>();
        orders3.add(new Order(323,"payment pending"));
        orders3.add(new Order(324,"delivered"));
        orders3.add(new Order(325,"on the way"));
        customers.add(new Customer(3,"John","Doe",7,700,new Contact(1434567890,"Flat No3,4th Main,Bangaalore"),70,orders3));
      	}

    public List<Customer> findAll() {
        return customers;
    }

    public Customer findById(int id) {
       for(Customer customer:customers)
       {
    	   if(customer.getId() == id)
    		   return customer;
       }
       return null;
    }


}
