package thoughtworks.vodqa.Customer;

import java.util.List;

public class Customer {
	
	int id;
	String fistName;
	String lastName;
	int cartQty;
	int cartTotal;
	Contact contact;
	int extracharges;
	List<Order> orders;
	
	public Customer(int id, String fistName, String lastName, int cartQty, int cartTotal, Contact contact, int extracharges, List<Order> orders) {
		super();
		this.id = id;
		this.fistName = fistName;
		this.lastName = lastName;
		this.cartQty = cartQty;
		this.cartTotal = cartTotal;
		this.contact=contact;
		this.extracharges = extracharges;
		this.orders = orders;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getfistName() {
		return fistName;
	}
	public void setfistName(String fistName) {
		this.fistName = fistName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getcartQty() {
		return cartQty;
	}
	public void setcartQty(int cartQty) {
		this.cartQty = cartQty;
	}
	public int getCartTotal() {
		return cartTotal;
	}
	public void setCartTotal(int cartTotal) {
		this.cartTotal = cartTotal;
	}


	public int getextracharges() {
		return extracharges;
	}
	public void setextracharges(int extracharges) {
		this.extracharges = extracharges;
	}
	public List<Order> getOrders() {
		return orders;
	}
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	public Contact getcontact() {
		return contact;
	}

	public void setcontact(Contact contact) {
		this.contact = contact;
	}
}
