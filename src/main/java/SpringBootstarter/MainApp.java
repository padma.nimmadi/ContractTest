package SpringBootstarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages={"thoughtworks.vodqa.Customer"})
public class MainApp {
	public static void main(String[] args)
	{
		SpringApplication.run(MainApp.class,args);
		System.out.println("*********Application has been Started by Padma Nimmadi**********");
	}
}
